import logo from './logo.svg';
import './App.css';
import Header from './BaiTapThucHanhLayOut/Header';
import Item from './BaiTapThucHanhLayOut/Item';
import Banner from './BaiTapThucHanhLayOut/Banner';
import Footer from './BaiTapThucHanhLayOut/Footer';
// import Body from './BaiTapThucHanhLayOut/Body';

function App() {
  return (
    <div className="App">
      <Header/>
      <Banner/>
      <Item/>
      <Footer/>
    </div>
  );
}

export default App;
